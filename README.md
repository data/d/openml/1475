# OpenML dataset: first-order-theorem-proving

https://www.openml.org/d/1475

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: James P Bridge, Sean B Holden and Lawrence C Paulson 
  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/First-order+theorem+proving)  

**Please cite**: James P Bridge, Sean B Holden and Lawrence C Paulson . Machine learning for first-order theorem proving: learning to select a good heuristic. Journal of Automated Reasoning, Springer 2012/13. 

Source:

James P Bridge, Sean B Holden and Lawrence C Paulson 

University of Cambridge 
Computer Laboratory 
William Gates Building 
15 JJ Thomson Avenue 
Cambridge CB3 0FD 
UK 

+44 (0)1223 763500 
forename.surname '@' cl.cam.ac.uk


Data Set Information:

See the file dataset file.


Attribute Information:

The attributes are a mixture of static and dynamic features derived from theorems to be proved. See the paper for full details.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1475) of an [OpenML dataset](https://www.openml.org/d/1475). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1475/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1475/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1475/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

